--ONLINE pages
select 
    case
        when extra_fields_in_json like '%strawman%' and charindex('"allowedParams"', extra_fields_in_json) = 0
            then 'accounts.chase.com/consumer/'+ offer_type +'/redirect/'+ email_ecoupon_prefix
        when extra_fields_in_json like '%strawman%' and charindex('"allowedParams"', extra_fields_in_json) > 0
            then 'accounts.chase.com/consumer/'+ offer_type +'/redirect/'+ email_ecoupon_prefix + '?'
            + replace(substring(extra_fields_in_json, charindex('"name"', extra_fields_in_json) + 8, 8), '"', '') + '='
            + replace(substring(extra_fields_in_json, charindex('"value"', extra_fields_in_json) + 9, 8),'"', '')
        when channel = 'FinnRAF'
            then 'accounts.chase.com/finnbank/friends/1234567'
        when channel = 'RAF'
            then 'accounts.chase.com/consumer/' + offer_type + '/online?px=' + email_ecoupon_prefix + '&key=1'
        else
            'accounts.chase.com/consumer/'+offer_type+'/online?px='+email_ecoupon_prefix
    end
as url, *      
from [jpmc].[jpmc_ecoupon].[offer]
where (is_offer_on is null or is_offer_on <>'0') and end_date > current_timestamp and et_email_version <> 'Executed by Chase'
order by end_date asc
 
--OFFLINE pages
select 'accounts.chase.com/consumer/'+ offer_type + '/offline?code=' + replace(offline_prefix, ' ', '') + 'XXXXXXXXX'
as url, *
from jpmc.jpmc_ecoupon.offline_offer
where (is_offer_on is null or is_offer_on <>'0') and end_date > current_timestamp
order by end_date asc
 
--NONOFFER pages
select
    case
        when static_name is not null
            then 'accounts.chase.com/consumer/' + offer_type + '/online/' + static_name
        when static_name is null
            then 'accounts.chase.com/consumer/' + offer_type + '/online?id=' + lib_id
    end
as url, *
from jpmc.jpmc_ecoupon.none_offer
where (is_offer_on is null or is_offer_on <>'0') and end_date > current_timestamp
order by end_date asc