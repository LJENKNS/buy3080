package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.Pages.StorePages.ItemPage.Checkout_Page_Template;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.testng.annotations.Test;


public class CheckoutScript extends BaseTest {


    @Test
    public void runScript() throws Exception {

        SeleniumHelper helper = new SeleniumHelper();

        //Log in to account
        Checkout_Page_Template template = new Checkout_Page_Template();

        template.load();

        helper.basicScroll(1000);
        Thread.sleep(1000);
        helper.waitUntilElementClickable(template.continueToPaymentButton.get(1), "Continue to Payment");
        template.continueToPaymentButton.get(1).click();

    }
}
