package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.Pages.StorePages.ItemPage.Cart_Page_Template;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.testng.annotations.Test;


public class CartScript extends BaseTest {


    @Test
    public void runScript() throws Exception {

        SeleniumHelper helper = new SeleniumHelper();

        //Log in to account
        Cart_Page_Template template = new Cart_Page_Template();

        template.load();

        Thread.sleep(1000);
        helper.waitUntilElementClickable(template.proceedToCheckoutButton, "Proceed to Checkout button");
        template.proceedToCheckoutButton.click();

    }
}
