package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.testng.annotations.Test;


public class Main extends BaseTest {

    @Test
    public void templateTest() throws Exception {

        //Setup
        //Make sure you have an account with gamestop
        //Make sure you have already set your home address
        //Make sure you have a credit card saved

        SeleniumHelper helper = new SeleniumHelper();

        LoginScript loginScript = new LoginScript();
        SearchAndBuyScript searchAndBuyScript = new SearchAndBuyScript();
        CartScript cartScript =  new CartScript();
        CheckoutScript checkoutScript = new CheckoutScript();
        PaymentScript paymentScript = new PaymentScript();
        PlaceOrderScript placeOrderScript =  new PlaceOrderScript();

        //Log in to account
        loginScript.runScript();
        //Search for product and, when its available, add it to cart
        searchAndBuyScript.runScript();
        //Cart Script
        cartScript.runScript();
        //Checkout Script
        checkoutScript.runScript();
        //Payment Script
        paymentScript.runScript();
        //Place order
        placeOrderScript.runScript();

        String testVar = null;



    }
}
