package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.Pages.StorePages.ItemPage.Search_Result_Page_Template;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;


public class SearchAndBuyScript extends BaseTest {

    Search_Result_Page_Template template = new Search_Result_Page_Template();
    @Test
    public void runScript() throws Exception {

        SeleniumHelper helper = new SeleniumHelper();

        //Load query page and press "add to cart"


        template.load();
        availabilityTest(template.adToCartButtons);


    }

    private void availabilityTest(List<WebElement> addToCartButtons)throws Exception {

        Boolean isAvalible = false;

        do {
            for (int x = 0; x < addToCartButtons.size(); x++) {
                String buttonText = addToCartButtons.get(x).getText();
                if (buttonText.equals("ADD TO CART")) {
                    addToCartButtons.get(x).click();
                    Thread.sleep(2000);
                    isAvalible = true;
                    System.out.println("Found one!");
                    break;
                }
                template.load();
            }
        }while (!isAvalible) ;
    }
}
