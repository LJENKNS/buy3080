package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.Pages.StorePages.ItemPage.Payment_Page_Template;
import com.axis.framework.main.util.GlobalConstants;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.testng.annotations.Test;


public class PaymentScript extends BaseTest {


    @Test
    public void runScript() throws Exception {

        SeleniumHelper helper = new SeleniumHelper();

        //Log in to account
        Payment_Page_Template template = new Payment_Page_Template();

        template.load();

        Thread.sleep(1000);
        template.securityCode.sendKeys(GlobalConstants.cvv);

        helper.basicScroll(1000);
        template.continueToOrderReviewButton.get(0).click();

    }
}
