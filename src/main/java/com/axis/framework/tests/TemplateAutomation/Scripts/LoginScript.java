package com.axis.framework.tests.TemplateAutomation.Scripts;

import com.axis.framework.main.Pages.StorePages.ItemPage.Home_Page_Template;
import com.axis.framework.main.util.GlobalConstants;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.tests.basetest.BaseTest;
import org.testng.annotations.Test;


public class LoginScript extends BaseTest {



    String itemUrl ="www.gamestop.com/video-games/switch/consoles/products/nintendo-switch-with-neon-blue-and-neon-red-joy-con/11095819.html?condition=New";
    String shippingUrl ="www.gamestop.com/checkout/?stage=shipping#shipping";


    @Test
    public void runScript() throws Exception {

        SeleniumHelper helper = new SeleniumHelper();

        //Log in to account
        Home_Page_Template homePageTemplate = new Home_Page_Template();
        homePageTemplate.load();

        Thread.sleep(1000);
        homePageTemplate.myAccountbutton.click();
        Thread.sleep(1000);
        homePageTemplate.signInButton.click();
        Thread.sleep(1000);
        homePageTemplate.modalEmailTextbox.sendKeys(GlobalConstants.emailAddress);
        homePageTemplate.modalPasswordTextBox.sendKeys(GlobalConstants.password);
        homePageTemplate.signInModalButton.click();
        Thread.sleep(5000);

    }
}
