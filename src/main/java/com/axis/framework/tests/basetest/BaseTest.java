package com.axis.framework.tests.basetest;

import com.axis.framework.main.browser.Browser;
import com.axis.framework.main.util.ReportLogging;
import com.axis.framework.main.util.SeleniumHelper;
import com.axis.framework.main.util.TestCaseHandler;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;


public class BaseTest{

    public static ReportLogging report;
    public static ExtentReports extent;
    public static ExtentTest logger;
    public static TestCaseHandler tch;

    public String suite;
    public String siteAndEnvironment;

    public WebDriver driver;

    public static SeleniumHelper helper;



    // Before suite occurs before all tests
    @BeforeSuite
    public void beforeSuite() throws Exception {
        report = new ReportLogging();
        extent = report.initTestReport(extent);

        helper = new SeleniumHelper();

        siteAndEnvironment = System.getProperty(("siteAndEnvironment"));
        suite = System.getProperty("suite");

 /*       if(siteAndEnvironment == null)
        {
            AppiumDriverLocalService service = AppiumDriverLocalService.buildDefaultService();
            service.start();
        }*/

    }

    @BeforeClass
    public void beforeClass(){

    }

    // Before method occurs before each test case
    @BeforeMethod
    public void beforeMethod() throws Exception{

        driver = Browser.initialize();
        Thread.sleep(1000);
    }

    @AfterMethod
    public void afterMethod() {
        Browser.close();
    }

    @AfterSuite
    public void afterSuite() throws Exception {
        report.endTestReport();

    }

}