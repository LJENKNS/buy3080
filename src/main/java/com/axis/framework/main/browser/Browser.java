package com.axis.framework.main.browser;


import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.MalformedURLException;
import java.net.URL;

import static com.axis.framework.main.util.Util.*;
import static org.openqa.selenium.remote.CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR;

public class Browser {
    public static WebDriver driver;

    public static WebDriver getDriver(String desiredBrowser) throws MalformedURLException {


        WebDriver newDriver = null;
        String driverPath = "src/main/java/com/axis/framework/drivers/";

/*        BrowserMobProxy proxy = new BrowserMobProxyServer();

        List<String> blackList = new ArrayList<String>();
        proxy.blacklistRequests("https://chaseuat.merkleservices.com/consumer/Scripts/common/modal_abandon-ADA.js",200);

        proxy.start();

        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);*/


        // Check OS and create driver path
        if (isMac()) { driverPath += "mac/"; }
        else if (isWindows()) { driverPath += "windows/"; }
        else if (isUnix()) { driverPath += "linux/"; }
        else System.out.println("OS not supported.");

        // get browser path and initialize browser driver
        switch (desiredBrowser) {
            case "firefox":

                System.setProperty("webdriver.gecko.driver", String.format("%sgeckodriver%s", driverPath, (isWindows() ? ".exe" : "")));
                FirefoxOptions firefoxOptions = new FirefoxOptions().addPreference("browser.link.open_newwindow", 3);
                //firefoxOptions.setCapability(CapabilityType.PROXY, seleniumProxy);

                newDriver = new FirefoxDriver(firefoxOptions);
                break;

            case "chrome":
                System.setProperty("webdriver.chrome.driver", String.format("%schromedriver%s", driverPath, (isWindows() ? ".exe" : "")));

                //System.setProperty("webdriver.chrome.driver", "/Users/brossiter/Automation/chase/src/main/java/com/axis/framework/drivers/mac/chromedriver");

                ChromeOptions options = new ChromeOptions();
                options.setCapability(UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

                newDriver = new ChromeDriver(options);
                break;

            case "safari":
                //NOTE: This is based off of: https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari
                System.setProperty("webdriver.safari.driver", String.format("/usr/bin/safaridriver"));
                SafariOptions safariOptions = new SafariOptions().setAutomaticInspection(true);
                newDriver = new SafariDriver(safariOptions);
                break;

            case "ie11":
                System.setProperty("webdriver.ie.driver", String.format("%sIEDriverServer.exe", driverPath));
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                internetExplorerOptions.setCapability("ignoreZoomSetting", true);
                newDriver = new InternetExplorerDriver(internetExplorerOptions);
                break;
            case "edge":
                EdgeOptions edgeOptions = new EdgeOptions();
                newDriver = new EdgeDriver(edgeOptions);
                break;
            case "android":
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("deviceName", "98895a333634524455");
                capabilities.setCapability("platformName", "Android");
                capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
                capabilities.setCapability("platformVersion", "9");
                capabilities.setCapability("appActivity","com.google.android.apps.chrome.ChromeTabbedActivity");

                try {
                    newDriver  = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                break;
        }

        // maximize browser window
        newDriver.manage().window().maximize();
        return newDriver;
    }

    public static WebDriver getRemoteDriver(String desiredBrowser) throws MalformedURLException {


        WebDriver newDriver = null;
        String driverPath = "src/main/java/com/axis/framework/drivers/";

/*        BrowserMobProxy proxy = new BrowserMobProxyServer();

        List<String> blackList = new ArrayList<String>();
        proxy.blacklistRequests("https://chaseuat.merkleservices.com/consumer/Scripts/common/modal_abandon-ADA.js",200);

        proxy.start();

        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);*/


        // Check OS and create driver path
        if (isMac()) { driverPath += "mac/"; }
        else if (isWindows()) { driverPath += "windows/"; }
        else if (isUnix()) { driverPath += "linux/"; }
        else System.out.println("OS not supported.");

        // get browser path and initialize browser driver

        String host = "localhost";

        if(System.getProperty("HUB_HOST") != null){
            host =  System.getProperty("HUB_HOST");

        }
        String completeURL = "http://" + host + ":4444/wd/hub";

        switch (desiredBrowser) {
            case "firefox":

                FirefoxOptions firefoxOptions = new FirefoxOptions().addPreference("browser.link.open_newwindow", 3);
                //firefoxOptions.setCapability(CapabilityType.PROXY, seleniumProxy);

                newDriver  = new RemoteWebDriver(new URL(completeURL), firefoxOptions);
                break;

            case "chrome":

                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setCapability(UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
                //chromeOptions.setCapability(CapabilityType.PROXY, seleniumProxy);

                //The options below are taken from https://stackoverflow.com/questions/48450594/selenium-timed-out-receiving-message-from-renderer
                // ChromeDriver is just AWFUL because every version or two it breaks unless you pass cryptic arguments

                chromeOptions.addArguments("start-maximized"); // https://stackoverflow.com/a/26283818/1689770
                chromeOptions.addArguments("enable-automation"); // https://stackoverflow.com/a/43840128/1689770

                newDriver  = new RemoteWebDriver(new URL(completeURL), chromeOptions);
                break;

            case "safari":
                //NOTE: This is based off of: https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari
                System.setProperty("webdriver.safari.driver", String.format("/usr/bin/safaridriver"));
                SafariOptions safariOptions = new SafariOptions().setAutomaticInspection(true);
                newDriver = new SafariDriver(safariOptions);
                break;

            case "ie11":
                System.setProperty("webdriver.ie.driver", String.format("%sIEDriverServer.exe", driverPath));
                InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
                internetExplorerOptions.setCapability("ignoreZoomSetting", true);
                newDriver = new InternetExplorerDriver(internetExplorerOptions);
                break;
            case "edge":
                EdgeOptions edgeOptions = new EdgeOptions();
                newDriver = new EdgeDriver(edgeOptions);
                break;
            case "android":
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability("deviceName", "98895a333634524455");
                capabilities.setCapability("platformName", "Android");
                capabilities.setCapability(CapabilityType.BROWSER_NAME, "Chrome");
                capabilities.setCapability("platformVersion", "9");
                capabilities.setCapability("appActivity","com.google.android.apps.chrome.ChromeTabbedActivity");

                try {
                    newDriver  = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                break;
        }

        // maximize browser window
        newDriver.manage().window().maximize();
        return newDriver;
    }

    public static WebDriver initialize() throws MalformedURLException {
        driver = getDriver(getDesiredBrowser());
        return driver;
    }

    public static WebDriver remoteInitialize() throws MalformedURLException {
        driver = getRemoteDriver(getDesiredBrowser());
        return driver;
    }

    public static WebDriver initializeChrome() throws MalformedURLException {
        driver = getDriver(getChromeBrowser());
        return driver;
    }

    public static void close() { driver.quit(); }

    // navigates to specified URL
    public static void goTo(String url) {
        driver.navigate().to(url);
    }
    public static void goToNewWindow(String url) {
        ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", url);
    }

}
