package com.axis.framework.main.testrails;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestRailsHandler {

    private final String TEST_RAILS_SERVER_NAME = "https://5thfinger.testrail.com/";
    private final String USERNAME = "awade@merkleinc.com";
    private final String PASSWORD = "Shoes112";
    private final String PROJECT_ID = "51";
    private APIClient client = new APIClient(TEST_RAILS_SERVER_NAME);

    public void init() {
        client.setUser(USERNAME);
        client.setPassword(PASSWORD);
    }

    // GET
    public Object request(String method) {
        Object c = null;
        try {
            c = client.sendGet(method);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        } {
        }
        return c;
    }

    // POST
    public void request(String method, Map data) {
        try {
            client.sendPost(method, data);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (APIException e) {
            e.printStackTrace();
        }
    }

    public void setTestCaseStatus(String runID, String caseID, int caseStatus, String comment) {
        Map data = new HashMap();
        data.put("comment", comment);
        data.put("status_id", caseStatus);
        request("add_result_for_case"+"/" + runID + "/" + caseID, data);
    }
}