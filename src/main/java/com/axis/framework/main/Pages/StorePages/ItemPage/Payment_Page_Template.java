package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Payment_Page_Template extends BasePage {

    String url = "www.gamestop.com/checkout/?stage=payment#payment";
    public Payment_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load() {
        loadTestPage(url);
    }


    @FindBy(id="saved-payment-security-code")
    public WebElement securityCode;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block submit-payment']")
    public List<WebElement> continueToOrderReviewButton;


}
