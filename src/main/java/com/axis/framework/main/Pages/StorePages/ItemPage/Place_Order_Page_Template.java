package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Place_Order_Page_Template extends BasePage {

    String url = "www.gamestop.com/checkout/?stage=placeOrder#placeOrder";
    public Place_Order_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load() {
        loadTestPage(url);
    }


    @FindBy(id="saved-payment-security-code")
    public WebElement securityCode;


    @FindBy(id="shipping-email")
    public List<WebElement> shippingEmail;

    @FindBy(id = "shippingFirstName")
    public List<WebElement> firstNameTextBox;

    @FindBy(id = "shippingLastName")
    public List<WebElement> lastNameTextBox;

    @FindBy(id = "shippingAddressOne")
    public List<WebElement> shippingAddressTextBox;

    @FindBy(id = "shippingState")
    public List<WebElement> shippingState;

    @FindBy(id = "shippingAddressCity")
    public List<WebElement> shippingAddressCity;

    @FindBy(id = "shippingZipCode")
    public List<WebElement> shippingZipCode;

    @FindBy(id = "shippingPhoneNumber")
    public List<WebElement> shippingPhoneNumber;

    @FindBy(xpath = "//*[@class='btn btn-primary btn-block place-order']")
    public List<WebElement> placeOrderButton;


}
