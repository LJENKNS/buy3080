package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cart_Page_Template extends BasePage {

    String url = "www.gamestop.com/cart/#payment";
    public Cart_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load() {
        loadTestPage(url);
    }

    @FindBy(xpath = "//*[@class='mb-2 mx-0 btn btn-primary btn-block checkout-btn ']")
    public WebElement proceedToCheckoutButton;


}
