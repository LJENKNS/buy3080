package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_Page_Template extends BasePage {

    String homepage = "www.gamestop.com";

    public Home_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load() {
        loadTestPage(homepage);
    }

    //------------------------Header-------------------------------
    @FindBy(id="account-modal-link")
    public WebElement myAccountbutton;

    @FindBy(id="signIn")
    public WebElement signInButton;

    @FindBy(id="login-form-email")
    public WebElement modalEmailTextbox;

    @FindBy(id="login-form-password")
    public WebElement modalPasswordTextBox;

    @FindBy(xpath = "//*[@class='btn btn-block btn-primary']")
    public WebElement signInModalButton;


}
