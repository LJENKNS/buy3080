package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Search_Result_Page_Template extends BasePage {

    String url = "www.gamestop.com/search/?q=3080&lang=default";
    //String url = "www.gamestop.com/search/?q=hello+kitty&lang=default";

    public Search_Result_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load() {
        loadTestPage(url);
    }

    //------------------------Header-------------------------------
    @FindBy(xpath="//*[@class='add-to-cart add-to-cart-plp btn btn-primary ']")
    public List<WebElement> adToCartButtons;

}
