package com.axis.framework.main.Pages.basepage;

import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.By;

import static com.axis.framework.main.util.SeleniumHelper.waitForElement;

public class BasePage {

    public String uatBaseUrl = "https://chaseuat.merkleservices.com/";
    public String stageBaseUrl = "https://account-uat.chase.com/";
    public String preProdBaseUrl = "http://hqsseiis80:8060/";
    public String app3BaseUrl = "http://hqsoachsapp03:8050/";
    public String productionBaseURL = "https://";

    public static String  environment = "prod";

    public void loadTestPage(String url){
        switch (environment){
            case "stage":
                goToStageUrl(url);
                break;
            case "uat":
                goToUATUrl(url);
                break;
            case "pre-prod":
                goToPreProd(url);
                break;
            case "prod":
                goToProdUrl(url);
                break;
            case "app3":
                goToApp3Url(url);
                break;
            default:
                goToProdUrl(url);
                break;
        }
    }
    public void loadNewStageLandingPage(String url){
        Browser.goTo("https://" + url);
    }

    public void goToProdUrl(String url) {
        Browser.goTo(productionBaseURL + url);
    }

    public void goToUATUrl(String url) {
        Browser.goTo(url);
    }

    public void goToStageUrl(String url) {
        Browser.goTo("https://jpmc:S@f3ty1nNumb3rz@" + url);
    }

    public void goToPreProd(String url) {
        String modifiedURL = url.substring(19,url.length());
        Browser.goTo(preProdBaseUrl + modifiedURL);
    }

    public void goToApp3Url(String url) {
        String modifiedURL = url.substring(19,url.length());
        Browser.goTo(app3BaseUrl + modifiedURL);
    }

    public void hardCodeURL(String url) {
        Browser.goTo(url);
        waitForElement(By.className("header"), 5);
    }

}