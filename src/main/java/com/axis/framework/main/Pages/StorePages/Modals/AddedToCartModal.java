package com.axis.framework.main.Pages.StorePages.Modals;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddedToCartModal extends BasePage {


    public AddedToCartModal() {
        PageFactory.initElements(Browser.driver, this);
    }

    //*[@id="addedToCartModal"]/div/div/div[2]/div[2]/a
    @FindBy(xpath ="//*[@id='addedToCartModal']//*[contains(@href, '#note2')]\"")
    public WebElement closeButton;

    @FindBy(xpath = "//*[@class='offer-pdf']/a")
    public WebElement collegeGuidePDF;
}