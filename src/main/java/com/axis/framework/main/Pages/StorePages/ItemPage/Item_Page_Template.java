package com.axis.framework.main.Pages.StorePages.ItemPage;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class Item_Page_Template extends BasePage {

    public Item_Page_Template() {
        PageFactory.initElements(Browser.driver, this);
    }

    public void load(String url) {
        loadTestPage(url);
    }

    //------------------------Header-------------------------------
    @FindBy(xpath = "//*[@id=\"primary-details\"]/div[4]/div[13]/div[2]/div/div[2]/a")
    public WebElement PlanDetailsLink;

    @FindBy(xpath = "//*[@class='add-to-cart btn btn-primary ']")
    public List <WebElement> buyNowButton;


}
