package com.axis.framework.main.Pages.Other;

import com.axis.framework.main.browser.Browser;
import com.axis.framework.tests.basetest.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

public class Extent_Report_Template extends BaseTest {

    public Extent_Report_Template() {
        PageFactory.initElements(Browser.driver,this);
    }

    public static File lastFileModified(String dir) {
        File fl = new File(dir);
        File[] files = fl.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                if(file.getName().contains(".html")){
                    choice = file;
                    lastMod = file.lastModified();
                }
            }
        }
        return choice;
    }

    @Test
    public void load() {

        String fileLocation = "Reports/";
        File getLastModifedFile = lastFileModified(fileLocation);
        String path = getLastModifedFile.getAbsolutePath();

        Browser.driver.get("file://" + path);
    }

    public String getMessage(String rawString){
        String modifiedString[] = rawString.split(" ",2);
        return modifiedString[1];
    }

    @FindBy(id = "test-collection")
    public WebElement parentTest;

    @FindBy(xpath = "//*[@class='mdi-navigation-cancel']")
    public WebElement filterToFailures;


    @FindBy(xpath = "//*[@id='test-details-wrapper']//*[@class='status fail']/..")
    public List<WebElement> failedChildTests;

    @FindBy(xpath = "//*[@id='test-collection']//*[(@class='collection-item test displayed fail') or (@class='collection-item test displayed fail active')]")
    public List<WebElement> failedParentTests;

    @FindBy(xpath = "//*[@id=\"test-details-wrapper\"]//*[@class='test-desc']")
    public WebElement testDescription;

    @FindBy(xpath = "//*[@id='test-collection']//*[(@class='collection-item test displayed pass') or (@class='collection-item test displayed pass active')]")
    public List<WebElement> passedParentTests;

    @FindBy(xpath = "//*[@id='test-collection']//*[(@class='collection-item test displayed pass') or (@class='collection-item test displayed pass active') or (@class='collection-item test displayed fail') or (@class='collection-item test displayed fail active')]//*[@class='test-name']")
    public List<WebElement> allParentTitles;
}
