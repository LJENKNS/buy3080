package com.axis.framework.main.util.CMS;

import com.axis.framework.main.Pages.basepage.BasePage;
import com.axis.framework.main.browser.Browser;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class CMSPage extends BasePage {
    public CMSPage() {
        PageFactory.initElements(Browser.driver, this);
    }

   public void load() {
        loadTestPage("jpmc:S@f3ty1nNumb3rz@account.chase.com/landing-pages");
    }

    /*public void load() {
        loadNewStageLandingPage("jpmc:S@f3ty1nNumb3rz@account-uat.chase.com/landing-pages");
    }*/

    //------------------------Header-------------------------------
    @FindBy(xpath = "//*[@class='views-field views-field-title']")
    public List<WebElement> rowTitles;

    @FindBy(xpath = "//*[@class='views-field views-field-title']/a")
    public List<WebElement> rowTitlesHref;

    @FindBy(xpath = "//*[@class='views-field views-field-view-node']")
    public List<WebElement> rowURLAlias;

    @FindBy(xpath = "//*[@class='views-field views-field-unpublish-on']")
    public List<WebElement> rowUnpublishedOn;

    @FindBy(xpath = "//*[@class='views-field views-field-field-swap-node']")
    public List<WebElement> rowSwapNode;

    @FindBy(xpath = "//*[@class='views-field views-field-field-code7']")
    public List<WebElement> rowDisplayCode;

    @FindBy(xpath = "//*[@class='views-field views-field-field-title']")
    public List<WebElement> rowECouponPrefix;

    public void alert(){

        Browser.driver.switchTo().alert();

        Actions act = new Actions(Browser.driver);

        act.sendKeys("jpmc");

        act.sendKeys(Keys.TAB).build().perform();

        act.sendKeys("S@f3ty1nNumb3rz");
    }

    public ArrayList<String> getTitles(){

        ArrayList<String> titleArrayList = new ArrayList<>();

        for (WebElement element: rowTitles) {
            titleArrayList.add(element.getText());
        }

        titleArrayList.remove(0);
        return titleArrayList;

    }

    public ArrayList<String> getUrls(){

        ArrayList<String> urlArrayList = new ArrayList<>();

        for (WebElement element: rowTitlesHref) {
            String rawText = element.getAttribute("href");
            String urlLink = rawText.substring(31);
            urlArrayList.add(urlLink);
        }

        return urlArrayList;

    }

    public ArrayList<String> getURLAlias(){

        ArrayList<String> codeArrayList = new ArrayList<>();

        for (WebElement element: rowURLAlias) {
            codeArrayList.add(element.getText());
        }

        codeArrayList.remove(0);
        return codeArrayList;

    }

    public ArrayList<String> getUnpublishedOn(){

        ArrayList<String> codeArrayList = new ArrayList<>();

        for (WebElement element: rowURLAlias) {
            codeArrayList.add(element.getText());
        }

        codeArrayList.remove(0);
        return codeArrayList;

    }

    public ArrayList<String> getSwapNode(){

        ArrayList<String> codeArrayList = new ArrayList<>();

        for (WebElement element: rowSwapNode) {
            codeArrayList.add(element.getText());
        }

        codeArrayList.remove(0);
        return codeArrayList;

    }

    public ArrayList<String> getDisplayCodes(){

        ArrayList<String> codeArrayList = new ArrayList<>();

        for (WebElement element: rowDisplayCode) {
            codeArrayList.add(element.getText());
        }

        codeArrayList.remove(0);
        return codeArrayList;

    }

    public ArrayList<String> getDisplayCode(){

        ArrayList<java.lang.String> displayCodeArrayList = new ArrayList<>();

        for (WebElement element: rowDisplayCode) {
            displayCodeArrayList.add(element.getText());
        }
        displayCodeArrayList.remove(0);

        return displayCodeArrayList;

    }

    public ArrayList<String> getECouponPrefix(){

        ArrayList<java.lang.String> eCouponPrefixArrayList = new ArrayList<>();

        for (WebElement element: rowECouponPrefix) {
            eCouponPrefixArrayList.add(element.getText());
        }

        eCouponPrefixArrayList.remove(0);
        return eCouponPrefixArrayList;

    }

}
