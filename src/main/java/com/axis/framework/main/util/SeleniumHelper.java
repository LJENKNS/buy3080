package com.axis.framework.main.util;

import com.axis.framework.main.browser.Browser;
import io.restassured.RestAssured;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.axis.framework.main.browser.Browser.driver;
import static com.axis.framework.tests.basetest.BaseTest.tch;
import static java.lang.Thread.sleep;

/*
 *
 * The Selenium Helper should be able to perform actions that occur globally on a site. If the method you are performing
 * does not serve a global purpose (more than one page template), please apply the method to a specific page object.
 *
 * Methods in the Selenium Helper should only be usable in test cases. If the methods are able to serve a purpose beyond
 * that, please check if Browser, ReportLogging or Util are a better fit for your method.
 *
 * If you find that you have a collection of SeleniumHelper methods that are only applicable to one global component (for
 * example, a header) look into creating a chunk object.
 *
 */

public class SeleniumHelper {

    public void resizeToiPhoneX(){
        Dimension dimension = new Dimension(375, 812);
        driver.manage().window().setSize(dimension);
    }

    public static WebElement findElement(By by) {
        WebElement element = driver.findElement(by);
        return element;
    }

    public static List<WebElement> findElements(By by) {
        List<WebElement> elements = driver.findElements(by);
        return elements;
    }

    public static String getPageTitle() {
        return driver.getTitle();
    }

    public static String getPageUrl() {
        return driver.getCurrentUrl();
    }

    public static void loadPage(String targetpage) { Browser.driver.navigate().to(targetpage);}




    public static void waitForElement(By by, int secondsToWait) {
        for (int i = 0; i < secondsToWait; i++) {
            if (elementOnPage(by)) {
                return;
            } else {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getBrowser(){
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();
        return browserName;
    }

    public String getOS(){
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String osName = cap.getPlatform().name().toLowerCase();
        return osName;
    }

    public static void waitForElements(int secondsToWaitForEach, By... elementLocators) {
        for (By element : elementLocators) {
            waitForElement(element, secondsToWaitForEach);
        }
    }

    public static boolean elementOnPage(By by) {
        return driver.findElements(by).size() > 0;
    }

    public static boolean elementOnPage(WebElement element) {
        return element.isDisplayed();
    }

    public static void waitAndDestroyElementBySelector(String selectorName) {
        if (elementOnPage(By.cssSelector(selectorName))) {
            javascript(String.format("var popup = document.querySelector('%s'); popup.parentNode.removeChild(popup);", selectorName));
            System.out.println("popup did appear");
        } else {
            System.out.println("popup did not appear");
        }
    }

    public void waitForPageLoadComplete(WebDriver driver, int specifiedTimeout) {
        WebDriverWait wait = new WebDriverWait(driver, specifiedTimeout);
        wait.until(driver1 -> String
                .valueOf(((JavascriptExecutor) driver1).executeScript("return document.readyState"))
                .equals("complete"));
    }

    public static ExpectedCondition<WebElement> steadinessOfElementLocated(final By locator) {
        return new ExpectedCondition<WebElement>() {

            private WebElement _element = null;
            private Point _location = null;

            @Override
            public WebElement apply(WebDriver driver) {
                if(_element == null) {
                    try {
                        _element = driver.findElement(locator);
                    } catch (NoSuchElementException e) {
                        return null;
                    }
                }

                try {
                    if(_element.isDisplayed()){
                        Point location = _element.getLocation();
                        if(location.equals(_location) && isOnTop(_element)) {
                            return _element;
                        }
                        _location = location;
                    }
                } catch (StaleElementReferenceException e) {
                    _element = null;
                }

                return null;
            }

            @Override
            public String toString() {
                return "steadiness of element located by " + locator;
            }
        };
    }

    public static boolean isOnTop(WebElement element) {
        WebDriver driver = ((RemoteWebElement)element).getWrappedDriver();

        return (boolean)((JavascriptExecutor)driver).executeScript(
                "var elm = arguments[0];" +
                        "var doc = elm.ownerDocument || document;" +
                        "var rect = elm.getBoundingClientRect();" +
                        "return elm === doc.elementFromPoint(rect.left + (rect.width / 2), rect.top + (rect.height / 2));"
                , element);
    }

    public void waitUntilElementInVisible(WebElement element,String elementName) throws Exception{
        try{
            WebDriverWait wait = new WebDriverWait(driver, 45);
            wait.until(ExpectedConditions.invisibilityOf(element));
        }catch(Exception ex){
            throw new Exception("waited 45 SEC but "+elementName+" is still visible");
        }

    }

    public void waitUntilElementVisible(WebElement element, String elementName)throws Exception {
        try{
            WebDriverWait wait = new WebDriverWait(driver, 45);
            wait.until(ExpectedConditions.visibilityOf(element));
        }catch(Exception ex){
            throw new Exception("waited 45 SEC but "+elementName+" is not visible");
        }
    }

    public void javaScriptClick(WebElement webElement){
        ((JavascriptExecutor)driver).executeScript("arguments[0].blur();", webElement);
        String javaScript = "arguments[0].click();";
        ((JavascriptExecutor)driver).executeScript(javaScript, webElement);
    }


    public WebDriverWait getWait(int sec){
        WebDriverWait wait = new WebDriverWait(driver, sec);
        return wait;
    }

    public void waitUntilElementClickable(WebElement element,String elementName) throws Exception{
        try{
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }catch(Exception ex){
            throw new Exception("waited 60 SEC but "+elementName+" is not clickable");
        }
    }

    public void waitForUrlContains(String expectedString, WebDriver driver, int specifiedTimeout) {
        WebDriverWait wait = new WebDriverWait(driver, specifiedTimeout);
        ExpectedCondition<Boolean> urlIsCorrect = arg0 ->    driver.getCurrentUrl().contains(expectedString);
        wait.until(urlIsCorrect);
        waitForPageLoadComplete(driver, specifiedTimeout);
    }

    public static void typeWithKeyModifier(Keys key, String query) {
        Actions shiftClick = new Actions(driver);
        shiftClick.keyDown(key).sendKeys(query).keyUp(key).perform();
    }

    public static void typeWithShift(String string) {
        typeWithKeyModifier(Keys.SHIFT, string);
    }

    public static void setScreenSize(String option) {
        switch (option) {
            // http://screensiz.es/
            case "mobile":
                // iPhone 7
                driver.manage().window().setSize(new Dimension(480, 1334));
                break;
            case "tablet":
                // iPad Air
                driver.manage().window().setSize(new Dimension(768, 1334));
                break;
            case "desktop":
                // full size window
                driver.manage().window().setSize(new Dimension(992, 1366));
                break;
            default:
                // full size window
                driver.manage().window().maximize();
                break;
        }
    }

    public static void setScreenSize(int width, int height) {
        driver.manage().window().setSize(new Dimension(width, height));
    }

    public static void fullscreen() {
        driver.manage().window().maximize();
    }

    public static void javascript(String script) {
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        jsx.executeScript(script);
    }

    public String javascriptReturnString(String script) {
        JavascriptExecutor jsx = (JavascriptExecutor) driver;
        String result = (String) jsx.executeScript(script);

        return result;
    }

    public static void pauseTest(int duration) {
        try {
            driver.wait(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void basicScroll(Integer scrollValue){
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0,arguments[0]);", scrollValue);
    }

    public void scrollToElement(WebElement element) throws Exception{
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public void chaseScrollToElement(WebElement element) throws Exception{
        Thread.sleep(500);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);" +
                "window.scrollBy(0, -200);", element);
        Thread.sleep(500);
    }

    public boolean newTabTest() throws Exception{
        int testVar = driver.getWindowHandles().size();

        if(Browser.driver.getWindowHandles().size()>1){
            return true;
        }
        return false;
    }

    public void highlightElement(WebElement element) throws Exception{
        scrollToElement(element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        //get the tab
        js.executeScript("arguments[0].style.border='10px groove green'", element);
    }

    public void hoverElement(WebElement webElement){
            String javaScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover'," +
                    "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
            ((JavascriptExecutor) driver).executeScript(javaScript, webElement);
}

    public void focusElement(WebElement webElement){
        ((JavascriptExecutor)driver).executeScript("arguments[0].blur();", webElement);
        String javaScript = "arguments[0].focus();";
        ((JavascriptExecutor)driver).executeScript(javaScript, webElement);
    }

    public void unFocusElement(WebElement webElement){
            ((JavascriptExecutor)driver).executeScript("arguments[0].blur();", webElement);
    }

    public void mouseOverElement(WebElement webElement){
        Actions action = new Actions(driver);
        action.moveToElement(webElement).perform();
        action.pause(100);
    }

    public void alert(){
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }



    public String[] displayedOffersToStringArray(List<WebElement> displayedOfferWebElements){

        List<String> displayedOffersStringList = null;

        for(int x=0;x<displayedOfferWebElements.size();x++){
                displayedOffersStringList.add(displayedOfferWebElements.get(x).toString());
        }
        String[] listToStringArray = (String[]) displayedOffersStringList.toArray();
        return listToStringArray;
    }


    public void goToNewWindow(){
        String MainWindow=driver.getWindowHandle();

        Set<String> s1=driver.getWindowHandles();
        Iterator<String> i1=s1.iterator();

        while(i1.hasNext())
        {
            String ChildWindow=i1.next();

            if(!MainWindow.equalsIgnoreCase(ChildWindow))
            {
                // Switching to Child window
                driver.switchTo().window(ChildWindow);

            }
        }
    }

    public void closeNewTab(String targetURL){

        String MainWindow=driver.getWindowHandle();

        Set<String> s1=driver.getWindowHandles();
        Iterator<String> i1=s1.iterator();

        while(i1.hasNext())
        {
            String ChildWindow=i1.next();

            if(!MainWindow.equalsIgnoreCase(ChildWindow))
            {

                // Switching to Child window
                driver.switchTo().window(ChildWindow);
                tch.condition(getPageUrl().contains(targetURL), "The new tab we opened contains the expected URL",
                        "Something is wrong with " + targetURL + " . I dont have the expected URL!");

                // Closing the Child Window.
                driver.close();
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
    }

    public void closeOldTab(){

        String MainWindow=driver.getWindowHandle();

        Set<String> s1=driver.getWindowHandles();
        Iterator<String> i1=s1.iterator();

        while(i1.hasNext())
        {
            String ChildWindow=i1.next();

            if(!ChildWindow.equalsIgnoreCase(MainWindow))
            {

                // Switching to Child window
                driver.switchTo().window(ChildWindow);

                // Closing the Child Window.
                driver.close();
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
    }


    public boolean imageErrorTester(WebElement imgElement) {
        try {
            highlightElement(imgElement);
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(imgElement.getAttribute("src"));
            HttpResponse response = client.execute(request);
            // verifying response code he HttpStatus should be 200 if not,
            // increment as invalid images count
            if (response.getStatusLine().getStatusCode() != 200)
                return true;
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

        public int httpResponseCode(String url) { return RestAssured.get(url).statusCode(); }

    public boolean waitFor200(String url) {

        if (httpResponseCode(url)==200){
            return true;
        }
        waitFor200(url);
        return false;
    }

    public String generateXPATH(WebElement childElement, String current) {
        String childTag = childElement.getTagName();
        if(childTag.equals("html")) {
            return "/html[1]"+current;
        }
        WebElement parentElement = childElement.findElement(By.xpath(".."));
        List<WebElement> childrenElements = parentElement.findElements(By.xpath("*"));
        int count = 0;
        for(int i=0;i<childrenElements.size(); i++) {
            WebElement childrenElement = childrenElements.get(i);
            String childrenElementTag = childrenElement.getTagName();
            if(childTag.equals(childrenElementTag)) {
                count++;
            }
            if(childElement.equals(childrenElement)) {
                return generateXPATH(parentElement, "/" + childTag + "[" + count + "]"+current);
            }
        }
        return null;
    }

    public String cssStyleConverter(WebElement webElement, String styleName){
        String rawStyle = webElement.getCssValue(styleName);
        String formatType = rawStyle.substring(0,4);

        switch (formatType){
            case "rgb(":
                String rgbColor = webElement.getCssValue(styleName);
                String hexFromRGB = Color.fromString(rgbColor).asHex();
                return hexFromRGB;
            case "rgba":
                String rgbaColor = webElement.getCssValue(styleName);
                String hexFromRGBA = Color.fromString(rgbaColor).asHex();
                return hexFromRGBA;
        }
        return webElement.getCssValue(styleName);
    }

    public void scrollTest(WebElement targetElement, String elementName){
        int targetYPosition = targetElement.getLocation().y;
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        Object currentYPositionObj = executor.executeScript("return window.scrollY;");

        long currentYPosition;

        if(currentYPositionObj instanceof Double){
            currentYPosition =  Math.round( (Double) currentYPositionObj);
        }
        else{
            currentYPosition = (long) currentYPositionObj;
        }

        Long result = (targetYPosition - currentYPosition);
        //YO THIS IS SOME GETTO CODE HERE.
        tch.condition((result)<1500 && currentYPosition!=0,
                "Clicking " + elementName + " has scrolled us to the correct position",
                "Clicking " + elementName + " has not scrolled us to the correct position");
    }

    public ArrayList<String> objectArrayToString(Object[] objects){
        ArrayList<String> strings = new ArrayList<>();

        for (Object o :
                objects) {
            String string = o.toString();
            strings.add(string);
        }
        return strings;
    }


    public void clickAndHold(WebElement element, String elementName)throws Exception{
        Actions actions = new Actions(Browser.driver);
        actions.moveToElement(element).perform();
        actions.moveByOffset(1,1).perform();

        actions.clickAndHold(element).perform();
        Thread.sleep(500);

        actions.release(element).perform();
    }



    public void chaseEmailErrorMessage(WebElement emailTextbox, WebElement emailErrorMessage){
        String colorValue = cssStyleConverter(emailErrorMessage, "color");

        String imagevalue = emailTextbox.getCssValue("background-image");

        tch.condition(cssStyleConverter(emailErrorMessage, "color").equals("#de0101"),
                ("Error message is the correct color red"),
        "Error message is not the correct color");
/*        tch.condition(emailTextbox.getCssValue("background-image").equals("url(\"https://d3dxof23bn91c6.cloudfront.net/dao/images/shared/invalid.png\")")
                || emailTextbox.getCssValue("background-image").equals("url(https://d3dxof23bn91c6.cloudfront.net/dao/images/shared/invalid.png)"),
                 "Components error message has the correct red 'i' png image",
                "Components error message does not have the correct red 'i' png image");*/
        tch.condition(emailErrorMessage.isDisplayed(),
                "The email error message is displayed",
                "The email error message is not displayed");
    }

    public String  chaseDateFormater(String rawDate){
        String year = rawDate.substring(0,4);
        String month = rawDate.substring(5,7);
        String day = rawDate.substring(8,10);

        return day + "/" + month + "/" + year;
    }

}