package com.axis.framework.main.util.Deets;

import com.axis.framework.main.Pages.Other.Extent_Report_Template;
import com.axis.framework.tests.basetest.BaseTest;
import com.google.common.collect.HashBasedTable;

import java.util.ArrayList;
import java.util.HashMap;


public class ExtentReportHelper extends BaseTest {

    public HashBasedTable<String, String,ArrayList<String>> gatherErrors() throws Exception{

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        HashBasedTable <String,String, ArrayList<String>> errorRows = HashBasedTable.create();

        extent_report_template.load();

        for(int x = 0;x< extent_report_template.failedParentTests.size();x++){
            String offerID = getOfferID(extent_report_template.failedParentTests.get(x).getText());
            String os = getOS(extent_report_template.failedParentTests.get(x).getText());
            String browser = getBrowser(extent_report_template.failedParentTests.get(x).getText());
            extent_report_template.failedParentTests.get(x).click();

            ArrayList<String> childErrors = listOfErrors();
            errorRows.put(offerID,os + " " + browser,childErrors);
        }
        return errorRows;
    }

    public HashMap<String, String> getFailedTestOfferIDs() {

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        ExtentReportHelper extentReportHelper = new ExtentReportHelper();

        HashMap<String,String> failedTestTemplateAndOfferIDMap = new HashMap<>();

        extent_report_template.load();

        for (int x = 0; x < extent_report_template.failedParentTests.size(); x++) {
            extent_report_template.failedParentTests.get(x).click();
            String url = extent_report_template.testDescription.getText();

            String offerID = extentReportHelper.getOfferID(extent_report_template.failedParentTests.get(x).getText());
            failedTestTemplateAndOfferIDMap.put(offerID,url);
        }
        return failedTestTemplateAndOfferIDMap;
    }

    public ArrayList<String> gatherPassedTests(){

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        ArrayList passedTests = new ArrayList();

        extent_report_template.load();

        for(int x = 0;x< extent_report_template.passedParentTests.size();x++){
            String rawPassedTestTitle = extent_report_template.passedParentTests.get(x).getText();
            String passedTestTitle = rawPassedTestTitle.substring(0,rawPassedTestTitle.length()-5);

            //String passedTestTitle = getOfferID(extent_report_template.passedParentTests.get(x).getText());

            passedTests.add(passedTestTitle);

        }
        return passedTests;
    }

    public ArrayList<String> gatherFailedTests(){

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        ArrayList passedTests = new ArrayList();

        extent_report_template.load();

        for(int x = 0;x< extent_report_template.failedParentTests.size();x++){
            String rawTestTitle = extent_report_template.failedParentTests.get(x).getText();
            String failedTestTitle = rawTestTitle.substring(0,rawTestTitle.length()-5);

            passedTests.add(failedTestTitle);

        }
        return passedTests;
    }

    public ArrayList<String> listOfErrors(){

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        ArrayList<String> failedTests = new ArrayList<String>();

        for(int x=0;x<extent_report_template.failedChildTests.size();x++){
            failedTests.add(extent_report_template.getMessage(extent_report_template.failedChildTests.get(x).getText()));
        }
        return failedTests;
    }

    public String getOfferID(String rawString){
        String offerID = rawString.substring(0, rawString.indexOf(":"));

        return offerID;
    }

    public String getTemplateType(String rawString){
        String templateType = rawString.substring(rawString.indexOf(":")+1);

        return templateType;
    }

    public String getOS(String rawString){
        String os = rawString.substring(rawString.indexOf(",")+2,rawString.indexOf(":"));

        return os;
    }

    public String getBrowser(String rawString){
        String broswer = rawString.substring(rawString.indexOf(":")+1,rawString.indexOf("Fail")-1);

        return broswer;
    }

    public HashBasedTable<String,String,String> summaryOfAllTestsTable()throws Exception{

        //driver = Browser.initialize();

        Extent_Report_Template extent_report_template = new Extent_Report_Template();

        extent_report_template.load();
            
        HashBasedTable<String,String,String> summaryOfAllTestsHashTable = HashBasedTable.create();
        
        ArrayList<String> rawTitles = new ArrayList<>();

        int parentTitleCount = extent_report_template.allParentTitles.size();

        for(int x=0;x<parentTitleCount;x++){
            rawTitles.add(extent_report_template.allParentTitles.get(x).getText());
        }

        ArrayList<String> passedTests = gatherPassedTests();
        ArrayList<String> failedTests = gatherFailedTests();

        for (String singleRawTitle : rawTitles) {

            String offerID = getOfferID(singleRawTitle);
            String templateType = getTemplateType(singleRawTitle);

            String testResult="-";

            if(passedTests.contains(singleRawTitle)){
                testResult="P";
            }
            else if(failedTests.contains(singleRawTitle)){
                testResult="F";
            }

            summaryOfAllTestsHashTable.put(offerID,templateType,testResult);

        }
        
        return summaryOfAllTestsHashTable;
    }
}
