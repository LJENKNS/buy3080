package com.axis.framework.main.util;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

/* Methods in this class should be related to the user's system or Maven.  */

public class Util {
    private static String OS = System.getProperty("os.name").toLowerCase();

    public static String currentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        Calendar cal = Calendar.getInstance();
        String dateTime = dateFormat.format(cal.getTime());
        return dateTime;
    }

    public static boolean isWindows() {

        return (OS.indexOf("win") >= 0);

    }

    public static boolean isMac() {

        return (OS.indexOf("mac") >= 0);

    }

    public static boolean isUnix() {

        return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );

    }

    public static File getTheNewestFile(String filePath, String ext) {
        File theNewestFile = null;
        File dir = new File(filePath);
        FileFilter fileFilter = new WildcardFileFilter("*." + ext);
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }

        return theNewestFile;
    }

    public static String getDesiredBrowser() {
        String browser = mavenParameter("browser");
        if (browser == null) {
            return GlobalConstants.defaultBrowser;
        }

        return browser;
    }

    public static String getChromeBrowser() {
        String browser = "chrome";

        return browser;
    }

    public static String mavenParameter(String propertyName) {
        return System.getProperty(propertyName);
    }
}
