package com.axis.framework.main.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import static com.axis.framework.main.util.EmailConstants.*;
import static com.axis.framework.main.util.Util.getTheNewestFile;

/* sends email for reporting, can be configured in EmailConstants file */
public class EmailReport {
    public void send() {

        File report = getTheNewestFile("/Users/awade/Desktop/cloned-axis41/axis41-selenium/Reports", "html");

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", EMAIL_SERVER);
        props.put("mail.smtp.port", EMAIL_PORT);
        props.put("mail.debug", "false");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(EMAIL_USERNAME, EMAIL_PASSWORD);
                    }
                });
        session.setDebug(true);

        try {

            Message message = new MimeMessage(session);
            MimeBodyPart reportAttachment = new MimeBodyPart();
            message.setFrom(new InternetAddress(EMAIL_USERNAME));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_RECIPIENTS_LIST));
            message.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(EMAIL_RECIPIENTS_CC_LIST));
            message.setSubject(EMAIL_SUBJECT);
            message.setText(EMAIL_MESSAGE);

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(reportAttachment);
            reportAttachment.attachFile(report);
            message.setContent(multipart);
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}