package com.axis.framework.main.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;

import static com.axis.framework.main.util.SeleniumHelper.findElement;

public class ElementInteraction {

    public static ArrayList<String> getElementSpacing(WebElement element, String spaceType) {
        String top = element.getCssValue(String.format("%s-top", spaceType));
        String right = element.getCssValue(String.format("%s-right", spaceType));
        String bottom = element.getCssValue(String.format("%s-bottom", spaceType));
        String left = element.getCssValue(String.format("%s-left", spaceType));
        String response = "%s:\ntop: %s | right: %s | bottom: %s | left: %s";

        System.out.println(String.format(response, spaceType, top, right, bottom, left));

        ArrayList<String> values = new ArrayList<String>();
        values.add(top);
        values.add(right);
        values.add(bottom);
        values.add(left);
        return values;
    }

    public static ArrayList<String> getElementSpacing(By by, String spaceType) {
        return getElementSpacing(findElement(by), spaceType);
    }

    public static ArrayList<String> padding(WebElement element) {
        return getElementSpacing(element, "padding");
    }

    public static ArrayList<String> padding(By by) {
        return getElementSpacing(findElement(by), "padding");
    }

    public static ArrayList<String> margin(WebElement element) {
        return getElementSpacing(element, "margin");
    }

    public static ArrayList<String> margin(By by) {
        return getElementSpacing(findElement(by), "margin");
    }

    public static String value(WebElement element) { return attribute(element, "value"); }

    public static String value(By by) { return findElement(by).getAttribute("value"); }

    public static String css(WebElement element, String cssValue) {
        return element.getCssValue(cssValue);
    }

    public static String css(By by, String cssValue) {
        return css(findElement(by), cssValue);
    }

    public static String attribute(WebElement element, String attributeValue) {
        return element.getAttribute(attributeValue);
    }

    public static String attribute(By by, String attributeValue) {
        return attribute(findElement(by), attributeValue);
    }

    public static void click(WebElement element) {
        element.click();
    };
}