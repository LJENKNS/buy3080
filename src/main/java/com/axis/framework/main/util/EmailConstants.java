package com.axis.framework.main.util;

public interface EmailConstants {

    /* EMAIL */
    // comma delimited
    boolean SEND_EMAIL_REPORT = false;

    String EMAIL_RECIPIENTS_LIST = "awade@merkleinc.com";
    String EMAIL_RECIPIENTS_CC_LIST = "";

    String EMAIL_USERNAME = "axis41.selenium.reports@gmail.com";
    String EMAIL_PASSWORD = "axis@selenium@.com";

    String EMAIL_SERVER = "smtp.gmail.com";
    String EMAIL_PORT = "587";

    String EMAIL_SUBJECT = "";
    String EMAIL_MESSAGE = "";

    /* TEST RAILS */
    boolean ENABLE_TEST_RAILS = true;
}