package com.axis.framework.main.util.SQLHelper;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * A Java MySQL SELECT statement example.
 * Demonstrates the use of a SQL SELECT statement against a
 * MySQL database, called from a Java program.
 *
 * Created by Alvin Alexander, http://alvinalexander.com
 */
public class SQLTest
{

    @Test
    public static void sqlLogin() throws SQLException {
        String connectionUrl = "jdbc:sqlserver://HQSCHSSQL01C03;databaseName=jpmc;user=pclc0\\brossiter;password=Patton90;integratedsecurity=true";

        Connection con = null;
        try {
            SQLServerDataSource dataSource = new SQLServerDataSource();
            dataSource.setUser("pclc0\\brossiter");
            dataSource.setPassword("Patton90");
            dataSource.setServerName("HQSCHSSQL01C03");
            dataSource.setDatabaseName("jpmc");

            con = dataSource.getConnection();
            Statement statement = con.createStatement();

            ResultSet resultSet = null;

            String selectSql = "SELECT TOP 10 * FROM jpmc_raf.targeted_customer;";
            resultSet = statement.executeQuery(selectSql);

            while (resultSet.next()) {
                System.out.println(resultSet.getString(2) + " " + resultSet.getString(3));
            }

            String testVar = "We made it!";
        }
        catch (SQLException e) {
                e.printStackTrace();
            }
    }
}