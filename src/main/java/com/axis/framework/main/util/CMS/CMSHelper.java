package com.axis.framework.main.util.CMS;

import com.axis.framework.tests.basetest.BaseTest;
import com.google.common.collect.HashBasedTable;
import org.testng.annotations.Test;

import java.util.ArrayList;



public class CMSHelper extends BaseTest {

    public HashBasedTable <String,String,String> generateLiveCMSPages(){
        CMSPage cmsPage = new CMSPage();
        cmsPage.load();

        ArrayList <String> titles =  cmsPage.getTitles();
        ArrayList <String> urls = cmsPage.getUrls();
        ArrayList <String> urlAlias = cmsPage.getURLAlias();
        ArrayList <String> unpublishedOn = cmsPage.getUnpublishedOn();
        ArrayList <String> swapNode = cmsPage.getSwapNode();
        ArrayList <String> displayCode = cmsPage.getDisplayCode();
        ArrayList <String> eCouponPrefix = cmsPage.getECouponPrefix();

        HashBasedTable <String,String,String> cmsHashBasedTable;

        cmsHashBasedTable = buildCMSHashBasedTable(urls,titles, urlAlias,unpublishedOn, swapNode,displayCode,eCouponPrefix);

        return cmsHashBasedTable;

    }

    public HashBasedTable <String,String,String> buildCMSHashBasedTable(ArrayList<String> urls, ArrayList<String> titles, ArrayList<String> urlAliases, ArrayList<String> unpublishedOns, ArrayList<String> swapNodes, ArrayList<String> displayCodes, ArrayList<String> eCouponPrefixes ){

        HashBasedTable <String,String,String> cmsHashBasedTable = HashBasedTable.create();

        int rowCount = urls.size();

        for(int x = 0;x < rowCount;x++){
            String url = urls.get(x);
            String title = titles.get(x);
            String urlAlias = urlAliases.get(x);
            String unpublishedOn = unpublishedOns.get(x);
            String swapNode = swapNodes.get(x);
            String displayCode = displayCodes.get(x);
            String eCouponPrefix = eCouponPrefixes.get(x);

            cmsHashBasedTable .put(urlAlias,"url", url);
            cmsHashBasedTable .put(urlAlias,"title", title);
            cmsHashBasedTable .put(urlAlias,"url alias", urlAlias);
            cmsHashBasedTable .put(urlAlias,"unpublished on", unpublishedOn);
            cmsHashBasedTable .put(urlAlias,"swap node", swapNode);
            cmsHashBasedTable .put(urlAlias,"display code", displayCode);
            cmsHashBasedTable .put(urlAlias,"Ecoupon Prefix", eCouponPrefix);

        }

        return cmsHashBasedTable;

    }

    @Test
    public void main() throws Exception {
        CMSHelper cmsHelper = new CMSHelper();

        cmsHelper.generateLiveCMSPages();



    }
}
