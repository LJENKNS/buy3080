package com.axis.framework.main.util.EmailHelpers;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Label;
import com.google.api.services.gmail.model.ListLabelsResponse;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import org.apache.commons.codec.binary.Base64;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class GmailHelper {
    private static final String APPLICATION_NAME = "Gmail API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES =  new ArrayList<>(List.of(GmailScopes.MAIL_GOOGLE_COM,
            GmailScopes.GMAIL_MODIFY,GmailScopes.GMAIL_READONLY));
    private static final String CREDENTIALS_FILE_PATH = "./GmailCredentials.json";

    //
    //

    /**
     * Creates an authorized Credential object.
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws Exception {
        // Load client secrets.
        InputStream in = new FileInputStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8000).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    /**
     * List all Messages of the user's mailbox matching the query.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param query String used to filter the Messages listed.
     * @throws IOException
     */
    public static List<Message> listMessagesMatchingQuery(Gmail service, String userId,
                                                          String query) throws IOException {
        ListMessagesResponse response = service.users().messages().list(userId).setQ(query).execute();

        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null) {
            messages.addAll(response.getMessages());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().messages().list(userId).setQ(query)
                        .setPageToken(pageToken).execute();
            } else {
                break;
            }
        }

/*        for (Message message : messages) {
            System.out.println(message.toPrettyString());
        }*/

        return messages;
    }

    /**
     * List all Messages of the user's mailbox with labelIds applied.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param labelIds Only return Messages with these labelIds applied.
     * @throws IOException
     */
    public static List<Message> listMessagesWithLabels(Gmail service, String userId,
                                                       List<String> labelIds) throws IOException {
        ListMessagesResponse response = service.users().messages().list(userId)
                .setLabelIds(labelIds).execute();

        List<Message> messages = new ArrayList<Message>();
        while (response.getMessages() != null) {
            messages.addAll(response.getMessages());
            if (response.getNextPageToken() != null) {
                String pageToken = response.getNextPageToken();
                response = service.users().messages().list(userId).setLabelIds(labelIds)
                        .setPageToken(pageToken).execute();
            } else {
                break;
            }
        }

/*        for (Message message : messages) {
            System.out.println(message.toPrettyString());
        }*/

        return messages;
    }

    // ...

    // ...


    /**
     * Get Message with given ID.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param messageId ID of Message to retrieve.
     * @return Message Retrieved Message.
     * @throws IOException
     */

    public static Message getMessage(Gmail service, String userId, String messageId)
            throws IOException {
        Message message = service.users().messages().get(userId, messageId).execute();

        System.out.println("Message snippet: " + message.getSnippet());

        return message;
    }

    /**
     * Get a Message and use it to create a MimeMessage.
     *
     * @param service Authorized Gmail API instance.
     * @param userId User's email address. The special value "me"
     * can be used to indicate the authenticated user.
     * @param messageId ID of Message to retrieve.
     * @return MimeMessage MimeMessage populated from retrieved Message.
     * @throws IOException
     * @throws MessagingException
     */
    public static MimeMessage getMimeMessage(Gmail service, String userId, String messageId)
            throws IOException, MessagingException {
        Message message = service.users().messages().get(userId, messageId).setFormat("raw").execute();

        Base64 base64Url = new Base64(true);
        byte[] emailBytes = base64Url.decodeBase64(message.getRaw());

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session, new ByteArrayInputStream(emailBytes));

        return email;
    }

    public static String getThing(String label, String thingStart, String thingEnd)throws Exception{
        MimeMessage email = getFirstEmailFromLabel(label);

        byte[] byteArr = email.getContent().toString().getBytes();
        String conversion = new String(byteArr);

        //System.out.println(conversion);

        String startOfAntiPhishingLink = thingStart;
        String endOfAntiPhishingLink = thingEnd;
        int indexOfAntiPhishingLinkStart = conversion.indexOf(startOfAntiPhishingLink)+startOfAntiPhishingLink.length();
        int indextOfAntiPhishingLinkEnd = conversion.indexOf(endOfAntiPhishingLink);

        String antiPhishingLink = conversion.substring(indexOfAntiPhishingLinkStart,indextOfAntiPhishingLinkEnd);
        return antiPhishingLink;

    }

    public static String getThingDesparate(String label, String pointToFind, int linkIndexValue)throws Exception{
        MimeMessage email = getFirstEmailFromLabel(label);

        byte[] byteArr = email.getContent().toString().getBytes();
        String conversion = new String(byteArr);

        //System.out.println(conversion);

        int indexOfPointToFind = conversion.indexOf(pointToFind);
        int indexOfVarianceCount = indexOfPointToFind + linkIndexValue;

        String antiPhishingLink;

        if(indexOfPointToFind<indexOfVarianceCount){
            antiPhishingLink = conversion.substring(indexOfPointToFind,indexOfVarianceCount);
        }
        else{
            antiPhishingLink = conversion.substring(indexOfVarianceCount,indexOfPointToFind);
        }
        return antiPhishingLink;

    }

    public static String getCouponCode(String query,String superQuery, String couponStart)throws Exception{
        MimeMessage email = getEmailFromSuperQuery(query,superQuery);

        byte[] byteArr = email.getContent().toString().getBytes();
        String conversion = new String(byteArr);

        //System.out.println(conversion);

        String startOfCoupon=couponStart;

        int indexOfCouponStart = conversion.indexOf(startOfCoupon);

        int indextOfCouponEnd = indexOfCouponStart+19;

        String couponCode = conversion.substring(indexOfCouponStart,indextOfCouponEnd);

        couponCode = couponCode.replaceAll("\\s+", "");
        return couponCode;

    }



    public static Gmail startGmailService() throws Exception {

        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Gmail service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();

        return service;

    }

    public static MimeMessage getFirstEmailFromLabel(String label)throws Exception{

        ArrayList<String> labelList = new ArrayList<>();
        labelList.add(label);
        Gmail service = startGmailService();
        //Get all the Amazon messages
        List <Message> xlobAmazonMessages = listMessagesWithLabels(service,"me",labelList);

        //Get the ID of the first message that matches the query
        String id = xlobAmazonMessages.get(0).getId();

        MimeMessage email = getMimeMessage(service,"me",id);

        return email;
    }

    public static MimeMessage getFirstEmailfromQuery(String searchQuery)throws Exception{

        Gmail service = startGmailService();
        //Get all the Amazon messages
        List <Message> messageList = listMessagesMatchingQuery(service,"me",searchQuery);

        //Get the ID of the first message that matches the query
        String id = messageList.get(0).getId();

        MimeMessage email = getMimeMessage(service,"me",id);

        return email;
    }

    public static MimeMessage getEmailFromSuperQuery(String searchQuery, String superQuery)throws Exception {

        Gmail service = startGmailService();
        //Get all the Amazon messages
        List<Message> messages = listMessagesMatchingQuery(service, "me", searchQuery);

        for (int x = 0; x < messages.size(); x++) {

            String id = messages.get(x).getId();

            MimeMessage email = getMimeMessage(service, "me", id);

            byte[] byteArr = email.getContent().toString().getBytes();
            String conversion = new String(byteArr);

            //System.out.println(conversion);

            if (conversion.contains(superQuery)) {
                return email;
            }
        }
        return null;
    }

    public static List<Label> getAllLabels(Gmail service) throws IOException {
        ListLabelsResponse response = service.users().labels().list("me").execute();
        List<Label> labels = response.getLabels();
        return labels;
    }

    public static String getLabelIDFromName(String query) throws Exception {
        Gmail service = startGmailService();
        List<Label> allLabels = getAllLabels(service);
        HashMap<String,String> labelsByName = new HashMap<>();

        for (Label singleLabel: allLabels) {
            String labelName = singleLabel.getName();
            String id = singleLabel.getId();
            labelsByName.put(labelName,id);
        }
        String labelId = labelsByName.get(query);

        return labelId;
    }

    public static void main(String[] args) throws Exception {
        Gmail gmailService = startGmailService();
        getAllLabels(gmailService);


    }
}