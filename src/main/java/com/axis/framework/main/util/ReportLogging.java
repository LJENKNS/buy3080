package com.axis.framework.main.util;


import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.axis.framework.main.util.Util.getDesiredBrowser;
import static com.axis.framework.tests.basetest.BaseTest.extent;

/* Before changing this file, make sure that you do not mean to change the TestCaseHandler object */

public class ReportLogging {
    public Screenshot screenshot = new Screenshot();

    public ExtentReports initTestReport(ExtentReports reportToInit) {
        String currentLocation = System.getProperty("user.dir");
        String time_stamp = new SimpleDateFormat("YY-MM-dd_HH-mm-ss").format(new Date());
        String reportFilePath = currentLocation + File.separator + "Reports" + File.separator + time_stamp + "_" + getDesiredBrowser() + ".html";
        reportToInit = new ExtentReports(reportFilePath, true);
        return reportToInit;
    }

    public ExtentTest initTestCaseReport(String offerID, String testType, String testDescription) {
        ExtentTest newLogger = extent.startTest(offerID + ":" + testType, testDescription);
        return newLogger;
    }

    public String generateTitle() {

        SeleniumHelper helper = new SeleniumHelper();

        String browser = helper.getBrowser();
        String os = helper.getOS();

        String testTitle = os +" " + browser;

        return testTitle;

    }

    public void endTestCaseReport(ExtentTest testToEnd) {
        extent.endTest(testToEnd);
        extent.flush();
    }

    public void endTestReport() {
        if (extent != null) {
            extent.close();
        }
    }
}
