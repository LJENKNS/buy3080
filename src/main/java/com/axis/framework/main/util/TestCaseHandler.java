package com.axis.framework.main.util;

import com.axis.framework.main.testrails.TestRailsHandler;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;

import static com.axis.framework.main.util.ElementInteraction.css;
import static com.axis.framework.main.util.EmailConstants.ENABLE_TEST_RAILS;

/* Before changing this file, make sure that you do not mean to change the SeleniumHelper object */

public class TestCaseHandler {

    public ExtentTest testCase;
    public TestRailsHandler testRails = new TestRailsHandler();
    public Screenshot screenshot = new Screenshot();

    private String runID;
    private String caseID;
    private boolean isFail = false;
    private String failReasons = "";

    public TestCaseHandler(ExtentTest testCase) {
        this.testCase = testCase;
        if (ENABLE_TEST_RAILS) {
            testRails.init();
        }
    }

    public void info(String details, Object... args) {
        testCase.log(LogStatus.INFO, String.format(details, args));
    }

    public void fail(String details, Object... args) {
        String failComment = String.format(details, args);
        testCase.log(LogStatus.FAIL, failComment);
        isFail = true;
        failReasons += "\n" +failComment;
    }

    public void pass(String details, Object... args) {
        String passComment = String.format(details, args);
        testCase.log(LogStatus.PASS, passComment);
    }

    public void error(String details, Object... args) {
        testCase.log(LogStatus.ERROR, String.format(details, args));
    }

    public void takeScreenshot(String statusType, String screenshotName) {
        String screenshotTaken = testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName));

        if (statusType.equals("info")) { info(testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName))); }
        else if (statusType.equals("fail")) { fail(testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName))); }
        else if (statusType.equals("error")) { error(testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName))); }
        else if (statusType.equals("pass")) { pass(testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName))); }
        else info(testCase.addBase64ScreenShot(screenshot.takeScreenShot(screenshotName)));
    }

    public void condition(Boolean condition, String passDetail, String failDetail) {
        if (condition) { pass(passDetail); }
        else  fail(failDetail);
    }

    public void condition(Boolean condition, String detail) {
        if (condition) { pass(detail); }
        else  fail(detail);
    }

    public void styleCheck(WebElement element, String cssAttribute, String expectedValue) {
        if (element == null) {
            error("Element returned null");
            return;
        }

        condition(
                css(element, cssAttribute).equals(expectedValue),
                String.format(
                        "%s of %s is %s. Expected Value: %s.",
                        cssAttribute, element.getTagName(), css(element, cssAttribute), expectedValue
                )
        );
    }

    public void styleMatch(WebElement elementOne, WebElement elementTwo, String cssAttribute) {
        if (elementOne == null) {
            error("Element one returned null");
            return;
        }
        if (elementTwo == null) {
            error("Element two returned null");
            return;
        }
        condition(
                css(elementOne, cssAttribute).equals(css(elementTwo, cssAttribute)),
                String.format(
                        "Element one: %s -- Element two: %s",
                        cssAttribute,
                        css(elementOne, cssAttribute),
                        css(elementTwo, cssAttribute)
                )
        );
    }

    public void javascriptExecutor(String script){

    }


    public void trReport(String runID, String caseID) {
        if (isFail) {
            testRails.setTestCaseStatus(runID, caseID, 5, String.format("Reasons for Fail: \n%s", failReasons));
        } else {
            testRails.setTestCaseStatus(runID, caseID, 1, "Test Passed.");
        }
    }
}
