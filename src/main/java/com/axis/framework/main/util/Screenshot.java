package com.axis.framework.main.util;

import com.axis.framework.main.browser.Browser;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* takes screenshots for reporting */

public class Screenshot {
    String screenShotPath = System.getProperty("user.dir")+ File.separator+"screenshots";

    public void captureScreenshot() throws Exception{
        String datetime = Util.currentDateTime();
        // Take screenshot and store as a file format
        File src= ((TakesScreenshot)Browser.driver).getScreenshotAs(OutputType.FILE);
        // Copy the  screenshot to desired location using copyFile //method
        FileUtils.copyFile(src, new File(screenShotPath+"\\Screenshot"+datetime+".png"));
    }

    public String takeScreenShot(String name) {
        File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
        String encodedBase64 = null;
        FileInputStream fileInputStreamReader = null;
        String time_stamp = Util.currentDateTime();
        String file_path = System.getProperty("user.dir")+File.separator+"screenshots"+File.separator+name+"-"+time_stamp+".png";
        try {
            FileUtils.copyFile(scrFile, new File(file_path));
            fileInputStreamReader = new FileInputStream(scrFile);
            byte[] bytes = new byte[(int)scrFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "data:image/png;base64,"+encodedBase64;
    }
}
